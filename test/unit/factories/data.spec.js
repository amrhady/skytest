describe('Factory: Data', function() {
  
	//will contain data from http response
	var movies;
	
	// Load the app module
	beforeEach(module('app'));
	
	beforeEach(inject(function(_data_, $httpBackend){
		//mock response
		$httpBackend.when('GET', 'data.json').respond(200, {movies:[{title:"test"}]});
	}));
	
	it('Loads the data', inject(function($httpBackend, data) {
		
		//load json ata
		data.getJson().then(function(d){
			movies = d.movies;
		});
		
		//test
		$httpBackend.expectGET('data.json');  
		
		//send the response. helps when testing asynchronous http calls
		$httpBackend.flush();
		
		//test
		expect(movies.length).toEqual(1);
	}));
	
});