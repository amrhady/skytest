describe('Filter: By', function() {

  var $filter;

  beforeEach(module('app'));
  
  beforeEach(inject(function(_$filter_){
    $filter = _$filter_;
  }));

  //filter returns filtered data.
  it('Returns filtered data', function() {
    var filter = $filter('byField');
    var results = filter(testdata.movies,'title',testdata.movies[0].title,3);
    expect(results.length).toEqual(1);
  });
	
  //filter returns all data if query is less than the minimum query length required for search.
  it('Returns all data when query less than min-length', function() {
    var filter = $filter('byField');
    var results = filter(testdata.movies,'title',"ab",3);
    expect(results.length).toEqual(testdata.movies.length);
  });
  
  //filter performs case insensitive search
  it('Performs case insensitive search', function() {
    var filter = $filter('byField');
    var resultsLowerCase = filter(testdata.movies,'title',testdata.movies[0].title.toLowerCase(),3);
    var resultsUpperCase = filter(testdata.movies,'title',testdata.movies[0].title.toUpperCase(),3);
    expect(resultsLowerCase.length).toEqual(1);
    expect(resultsLowerCase.length).toEqual(resultsUpperCase.length);
  });
});

