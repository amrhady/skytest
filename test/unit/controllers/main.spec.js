describe('Controller: MainCtrl', function(){

	var $controller;
	var scopeReference;
	var dataFactory;
	var MockFilters;

	//mock factory and filters
    beforeEach(module('app', function($provide) {
	    $provide.factory('data', function() {
			return{
				getJson:function(){
					return {
						then:function(f){
							f(testdata);
						}
					}
				}
			}
		});
		$provide.value('myfilters', function(s) {
			var f;
			if (s=='byField'){
				return function(items,field,value,min){
			   	    if (value.trim().length<min) return items;
			        var filtered = [];
			        for (i in items){
				        if (items[i][field] && items[i][field].toLowerCase().indexOf(value.toLowerCase())>= 0){
						    filtered.push(items[i]);
					    }
				    }
				   	return filtered;
		        };
			}
		});  
	}));
	
  
	//inject our MainCtrl, factory, and filters.
	beforeEach(inject(function($rootScope, _$controller_, data, myfilters){
		$controller = _$controller_;
		scopeReference = $rootScope;
		dataFactory = data;
		MockFilters = myfilters;
	}));

	
	
	describe('Initialize controller', function() {
	
		it('Loads the movie data', function() {
	    	//init controller.
			var controller = $controller('MainCtrl', { $scope: scopeReference, data:dataFactory, $filter:MockFilters  });
			//test movies are loaded
			expect(scopeReference.movies.length).toEqual(160);
		});
	    
		it('Updates limit on search query update', function() {
			//init controller
			var controller = $controller('MainCtrl', { $scope: scopeReference, data:dataFactory, $filter:MockFilters  });
			
			//test limit before updating search value
			expect(scopeReference.search.limit).toEqual(20);
			
			//change the slider value
			scopeReference.search.query = "test";
			
			//trigger watch 
			scopeReference.$digest();
			
			//test limit before updating search value
			expect(scopeReference.search.limit).toEqual(scopeReference.movies.length);
			
		});
	});
});