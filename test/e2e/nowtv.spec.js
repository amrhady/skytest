describe('can visit nowtv.com', function(){

    beforeEach( function(){
        browser.driver.get('http://localhost:3000');
    });

    it('user can successfully navigate to nowtv.com', function(){
        expect( browser.driver.getCurrentUrl() ).toContain('localhost');
    });
});