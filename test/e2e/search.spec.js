describe('can search movies on nowtv.com', function(){

    beforeEach( function(){
        browser.driver.get('http://localhost:3000/');
    });

    it('Loads movis on page load', function(){
        expect(element.all(by.css('.movie')).count()).toEqual(20);
    });
    
    it('Display correct movie metadata on page load', function(){
        expect(element.all(by.css('.movie .title')).count()).toEqual(20);
        expect(element.all(by.css('.movie .year')).count()).toEqual(20);
        expect(element.all(by.css('.movie .duration')).count()).toEqual(20);
        expect(element.all(by.css('.movie .rating')).count()).toEqual(20);
        expect(element.all(by.css('.movie .actors')).count()).toEqual(20);
        expect(element.all(by.css('.movie .actors .actor')).count()).toBeGreaterThan(0);
    });
    
    it('Displays minimum search character message', function(){
	    search = element(by.model('search.query'));
	    search.sendKeys('ab');
	    searchmessage = element(by.css('.search-message'));
	    expect(searchmessage.getText()).toEqual('Enter at least three characters to begin search');
	    search.clear();
    });
    
    it('Displays no results message', function(){
	    search = element(by.model('search.query'));
	    search.sendKeys('abcxyz123');
	    searchmessage = element(by.css('.search-message'));
	    expect(searchmessage.getText()).toEqual('No matching items');
	    search.clear();
    });
    
    it('Display correct search results', function(){
	    var data = element(by.repeater('movie in search.results').row(0).column('movie.title'));
	    search = element(by.model('search.query'));
	    search.sendKeys(data.getText());
	    expect(element.all(by.css('.movie')).count()).toEqual(1);
	    expect(element.all(by.css('.movie .title')).get(0).getText()).toEqual(data.getText());
    });
    
    

});