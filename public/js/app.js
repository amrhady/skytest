(function() {

    var app = angular.module('app', []);
    
    app.constant('CONSTANTS',{
	    ENTER_MIN_SEARCH_CHARS:'Enter at least three characters to begin search',
	    NO_RESULTS: 'No matching items',
	    MIN_SEARCH_CHARS:3,
	    MAX_RESULTS_ON_EMPTY_SEARCH: 20
	});

    app.controller('MainCtrl', ['$scope', 'data', 'CONSTANTS', function($scope, data, CONSTANTS) {
	    
	    // use Math functions
	    $scope.Math = window.Math;
	    
	    // constants
	    $scope.search = {
		    query: '',
		    limit:CONSTANTS.MAX_RESULTS_ON_EMPTY_SEARCH,
		    minChars: CONSTANTS.MIN_SEARCH_CHARS,
		    minCharsMsg:CONSTANTS.ENTER_MIN_SEARCH_CHARS,
		    noResultsMsg: CONSTANTS.NO_RESULTS
		};
	
	    // watch changes in search query
		$scope.$watch("search.query", function(v){
		    $scope.search.limit = v.length<CONSTANTS.MIN_SEARCH_CHARS?CONSTANTS.MAX_RESULTS_ON_EMPTY_SEARCH:$scope.movies.length;
	    });
	    
	   	
		// load data
    	data.getJson().then(function(d){
	   		$scope.movies = d.movies;
	    }); 
	    
    }]);
    
    // filter array of objects by specific field and corresponding value
   	app.filter('byField', function() {
   	    return function(items,field,value,min){
	   	    if (value.trim().length<min) return items;
	        var filtered = [];
	        for (i in items){
		        if (items[i][field] && items[i][field].toLowerCase().indexOf(value.toLowerCase())>= 0){
				    filtered.push(items[i]);
			    }
		    }
		   	return filtered;
        };
	});
	
	// data factory
    app.factory('data', ['$http', function($http) {
        return {
            getJson: function() {
                return $http.get('data.json')
                    .then(
                        function(response) {
                            return response.data;
                        },
                        function(httpError) {
                            throw httpError.status + " : " + httpError.data;
                        });
            }
        }
    }]);
})();
